package AST;

import java.util.ArrayList;
import Gramatica.Gramatica;
import Gramatica.Regla;
import Gramatica.Symbol;
/**
 * Clase que genera el conjunto de estados necesario para la aplicacion del algoritmo SLR.
 * @author Hector Gonzalez Guerreiro
 *
 */
public class MaquinaEstados {
	private ArrayList<Conjunto> I = new ArrayList<>();
	private Gramatica gramatica;
	
	/**
	 * Constructor que inicia el algoritmo.
	 * @param grama	El conjunto de reglas que vamos a analizar.
	 */
	public MaquinaEstados(Gramatica grama) {
		this.gramatica = grama;
		Conjunto CR = new Conjunto();
		CR.insertarReglas(gramatica.getRegla(0));
		CR.Iclausura(gramatica);
		I.add(CR);
		Setup();
	}
	/**
	 * A cada conjuto  lo analizo, termino cuando me quede sin conjunto que analizar (FINALIZA EL ALGORITMO)
	 */
	public void Setup() {
		for (int Indice = 0; Indice < I.size(); Indice++) {
			AnalizarConjuntos(I.get(Indice));
		}
	}
	/**
	 * Analizo un conjunto genreando si es posible otros conjuntos con reglas o aristas si ocurre la ocasion.
	 * El algoritmo analiza todas las rglas que este pose  y las consume, este proceso provoca diferentes acciones como consecuencia de consumir un symbolo que
	 * el sistema tiene que tener en cuenta.
	 *  - Si se puede consumir pero el elemento a cosnumir es un eof creo una transicion de final indicado que esta transicion da un OK.
	 *  - Si por lo contrario la regla existe en algun conjunto y ya tengo la transicion simplemte inserto la regla a ese conjunto. 
	 *  - si no existiese esa transicion la creo.
	 *  - si la regla no se encuentra en ningon conjunto y existe una trasicion que se asocie al symbolo consumido la busco he inserto la regla al conjunto asociado a ella.
	 *  - por ultimo si no existe ni el conjunto ni arista creo un nuevo conjunto con la transicion y a�ado ese conjunto a mi array de estados.
	 *  
	 * @param CR le paso un conjunto.
	 */
	public void AnalizarConjuntos(Conjunto CR) {
		Regla R = null;
		Regla RC = null;
		Symbol sys = null;
		Arista aris = null;
		for (int Indice_R = 0; Indice_R < CR.numR(); Indice_R++) {
			R = CR.getRegla(Indice_R);
			if (R.se_consume()) {
				sys = R.Elemento_Consumido();
				RC = R.Consumir();
				if (!sys.esAceptar()) {
					if (existeRegla(RC) > 0) {
						if (CR.existeArista(sys, CR)) {
							aris = CR.getAristas(sys);
							aris.getConjunto().insertarReglas(RC);
							aris.getConjunto().Iclausura(gramatica);
						} else {
							int posicion_conjunto = existeRegla(RC);
							aris = new Arista(sys, I.get(posicion_conjunto));
							CR.insertarArista(sys, I.get(posicion_conjunto));
							I.get(posicion_conjunto).insertarReglas(RC);
							I.get(posicion_conjunto).Iclausura(gramatica);
						}
					} else {
						if (CR.getAristas(sys) != null) {
							aris = CR.getAristas(sys);
							aris.getConjunto().insertarReglas(RC);
							aris.getConjunto().Iclausura(gramatica);
						} else {
							Conjunto CRN = new Conjunto();
							CRN.insertarReglas(RC);
							CR.insertarArista(sys, CRN);
							CRN.Iclausura(gramatica);
							I.add(CRN);
						}
					}
				} else {
					aris = new Arista();
					CR.insertarAristaFinal(aris);
				}
			}
		}
	}

	public int existeRegla(Regla R) {
		for (int index = 0; index < I.size(); index++) {
			if (I.get(index).existeRegla(R)) {
				return index;
			}
		}
		return -1;
	}

	public String toString() {
		String salida = " ";
		for (int i = 0; i < I.size(); i++) {
			salida = salida + I.get(i).toString();
		}
		return salida;
	}

	public int numConjuntos() {
		return I.size() ;
	}

	public Conjunto getConjunto(int index) {
		return I.get(index);
		
	}

}
