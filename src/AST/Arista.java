package AST;

import Gramatica.Symbol;
import Gramatica.Terminal;
import Lexico.Token;

public class Arista {
	private Symbol elemento_consumido;
	private Conjunto Sig;
	private Boolean esFinal;
	
	public Arista(Symbol sy, Conjunto Sig){
		this.elemento_consumido = sy;
		this.Sig = Sig;
		this.esFinal = false;
	}
	
	public Arista(){
		Token end = new Token(Token.EOF, "<EOF>", 0, 0);
		this.elemento_consumido = new Terminal(end);
		this.Sig = null;
		this.esFinal = true;
	}
	
	public Boolean esFinal(){
		return this.esFinal;
	}

	public Token getToken() {
		return elemento_consumido.getToken();
	}
	
	public Symbol getSymbol(){
		return elemento_consumido;
	}
	
	
	public void setToken(Token T) {
		elemento_consumido.setToken(T);
	}

	public boolean equals(Symbol s) {
		return elemento_consumido.equals(s);
	}

	public Conjunto getConjunto() {
		return Sig;
		
	}
	
	public String toString(){
		return elemento_consumido.toString() + " -> " + Sig.getId();
	}
	
	
}
