package AST;

import java.util.ArrayList;

import Gramatica.Gramatica;
import Gramatica.NoTerminal;
import Gramatica.Regla;
import Gramatica.Symbol;
/**
 * Conjunto alamcena reglas y transciones
 * @author Alistair
 *
 */
public class Conjunto {
	private ArrayList<Regla> Reglas = new ArrayList<>();
	private ArrayList<Arista> Aris = new ArrayList<>();
	private int id;
	public static int num_estado;

	public Conjunto() {
		this.id = num_estado;
		num_estado++;
	}
	/**
	 * Inserta una regla que no este repetida
	 * @param R regla
	 */
	public void insertarReglas(Regla R) {
		if (!this.existeRegla(R))
			Reglas.add(R);
	}
	/**
	 * Inserta un conjunto de reglas 
	 * @param R Conjunto de reglas.
	 */
	public void insertarConjuntoR(ArrayList<Regla> R) {
		for (int index = 0; index < R.size(); index++) {
			insertarReglas(R.get(index));
		}
	}
	/**
	 * 
	 * @param R regla
	 * @return true si existe la regla /false si no existe esa regla en el conjunto
	 */
	public boolean existeRegla(Regla R) {
		for (int index = 0; index < Reglas.size(); index++) {
			if (Reglas.get(index).equals(R))
				return true;
		}
		return false;
	}
	/**
	 * 
	 * @param sys symbol 
	 * @return true si existe alguna regla con el encabezado que indique el parametro sys.
	 */
	public boolean existeEncabezado(Symbol sys) {
		for (int index = 0; index < Reglas.size(); index++) {
			if (Reglas.get(index).getEncabezado().equals(sys))
				return true;
		}
		return false;
	}
	/**
	 * Inserto una arista o transicion 
	 * @param consumido	Symbol que fue consumido.
	 * @param c	Conjunto destino de la transicion.
	 */
	public void insertarArista(Symbol consumido, Conjunto c) {
		if (!existeArista(consumido,c)) {
			Arista a = new Arista(consumido, c);
			Aris.add(a);
		}
	}
	/**
	 * True si existe alguna arista que consuma un symbolo y ademas tenga como destino de la transicion el parametro c
	 * @param consumido Elemento Symbol
	 * @param c	Conjunto
	 * @return
	 */
	public boolean existeArista(Symbol consumido, Conjunto c) {
		for (int index = 0; index < Aris.size(); index++) {
			if (Aris.get(index).equals(consumido) && c.getId() != Aris.get(index).getConjunto().getId())
				return true;
		}
		return false;
	}
	/**
	 * 
	 * @param co symbolo
	 * @return	Arista que contenga el symbolo pasado por parametro.
	 */
	public Arista getAristas(Symbol co) {
		for (int index = 0; index < Aris.size(); index++) {
			if (Aris.get(index).equals(co))
				return Aris.get(index);
		}
		return null;
	}
	/**
	 * 
	 * @param co Symbolo
	 * @return	Conjunto que tenga asociado el siguiente en una arista que tenga como symbolo el pasado por parametro.
	 */
	public Conjunto getSig(Symbol co) {
		for (int index = 0; index < Aris.size(); index++) {
			if (Aris.get(index).equals(co))
				return Aris.get(index).getConjunto();
		}
		return null;
	}
	/**
	 * Ejecuta el Iclausura de un conjunto.
	 * @param g
	 */
	public void Iclausura(Gramatica g) {
		for(int i = 0; i < Reglas.size(); i++)
			Iclausura(g, Reglas.get(i));
	}

	private void Iclausura(Gramatica g, Regla R) {
		Symbol sys = null;
		ArrayList<Regla> Raux = new ArrayList<>();

		if (R.se_consume()) {
			sys = R.Elemento_Consumido();
			if (!existeEncabezado(sys) && sys instanceof NoTerminal) {
				Raux = g.ConjuntoR((NoTerminal) sys);
				insertarConjuntoR(Raux);
			}
			else if(this.existeRegla(R) && sys instanceof NoTerminal){
				Raux = g.ConjuntoR((NoTerminal) sys);
				insertarConjuntoR(Raux);
			}
		}

	}
	/**
	 * numero de reglas que contine un conjunto.
	 * @return
	 */
	public int numR() {
		return Reglas.size();
	}
	
	public Regla getRegla(int indice) {
		return Reglas.get(indice);
	}

	public int getId() {
		return id;
	}

	public String toString() {
		String salida = "ID: " + id + " {\n";
		for (int index = 0; index < Reglas.size(); index++) {
			salida = salida + "\t" + Reglas.get(index).toString() + " \n";
		}
		for (int index = 0; index < Aris.size(); index++) {
			if(!Aris.get(index).esFinal())
				salida = salida + "\t" + Aris.get(index).toString() + "\n";
			else 
				salida = salida + "\t" + "Aceptar" + "\n";
		}

		salida = salida + "} \n";

		return salida;
	}

	static {
		num_estado = 0;
	}

	public void insertarAristaFinal(Arista aris) {
		Aris.add(aris);
	}

	public int numTrans() {
		return Aris.size();
	}

	public Arista getAristas(int trans) {
		return Aris.get(trans);
	}

}
