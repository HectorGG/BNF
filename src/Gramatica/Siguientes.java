package Gramatica;

import java.util.ArrayList;

public class Siguientes {
	private ArrayList <Terminal>  Siguientes = new ArrayList<>();
	private Gramatica gramatica;
	
	public Siguientes(Gramatica gramatica, NoTerminal N){
		this.gramatica = gramatica;
		ArrayList <NoTerminal>  NT = new ArrayList<>();
		setup(N,NT);
		
	}

	private void setup(NoTerminal symbol, ArrayList<NoTerminal> nT) {
		for(int index = 0; index < gramatica.numReglas(); index++){
			Regla R = gramatica.getRegla(index);
			for(int ele = 0; ele < R.numEl(); ele++){
				if(symbol.equals(R.getElemento(ele))){
					if(ele + 1 < R.numEl()){											// QUIERE DECIR QUE PUEDO VER EL SIGUIENTE //
						if(R.getElemento(ele + 1) instanceof Terminal){
							this.insertarSiguientes((Terminal) R.getElemento(ele + 1));
						}
						
						else if(R.getElemento(ele + 1) instanceof NoTerminal){
							Primeros pr = new Primeros(gramatica);
							pr.primeros(symbol);
							this.insertarPrimeros(pr.getPrimeros());
						}
					}
					else{
						if(!estaVisto((NoTerminal) R.getEncabezado(), nT)){
							nT.add((NoTerminal) R.getEncabezado());
							setup((NoTerminal) R.getEncabezado(), nT);
						}
					}
					
				}
			}
		}
		
	}
	
	public void insertarSiguientes(Terminal ter){
		if(!Siguientes.contains(ter)){
			Siguientes.add(ter);
		}
	}
	
	public void insertarPrimeros(ArrayList<Terminal> list){
		for(int index = 0; index < list.size(); index++){
			insertarSiguientes(list.get(index));
		}
	}
	
/*	public void visto(NoTerminal NTER){
		if(!NT.contains(NTER)){
			NT.add(NTER);
		}
	}*/
	
	public boolean  estaVisto(NoTerminal NTER, ArrayList<NoTerminal> Nt){
		int pos = 0;
		while(pos < Nt.size()){
			if(NTER.equals(Nt.get(pos)))
				return true;
			else
				pos++;
		}
		return false;
	}
	
	public ArrayList<Terminal> getSiguiente(){
		return Siguientes;
	}
}
