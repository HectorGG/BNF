package Gramatica;

import java.util.ArrayList;

import Lexico.Token;

public class Gramatica {
	private ArrayList<Symbol> ListaNoTerminales = new ArrayList<>();
	private ArrayList<Symbol> ListaTerminales = new ArrayList<>();
	private ArrayList<Regla>  ListaReglas = new ArrayList<>();
	
	/**
	 * A�ado terminal final de fichero.
	 */
	public Gramatica(){
		ListaTerminales.add(new Terminal(new Token(Token.EOF, "<EOF>", 0, 0)));
	}
	
	/**
	 * Este constructor ademas de insertar las reglas inserta una en primer lugar la <X> -> <A> EOF
	 * @param parseDefinicion_prima Conjunto de reglas que van a ser cargadas en la estructura de datos.
	 */
	public Gramatica(ArrayList<Regla> parseDefinicion_prima) {
		
		ListaTerminales.add(new Terminal(new Token(Token.EOF, "<EOF>", 0, 0)));
		insertarReglas( new Regla(parseDefinicion_prima.get(0).getEncabezado(), 1));
		
		for(int index = 0; index < parseDefinicion_prima.size(); index++){
			insertarNoTerminal(parseDefinicion_prima.get(index).getEncabezado());
			for(int index_2 = 0; index_2 < parseDefinicion_prima.get(index).numEl(); index_2++){
				if(parseDefinicion_prima.get(index).getElemento(index_2) instanceof Terminal)
					insertarTerminal(parseDefinicion_prima.get(index).getElemento(index_2));
				else
					insertarNoTerminal(parseDefinicion_prima.get(index).getElemento(index_2));
			}
			insertarReglas(parseDefinicion_prima.get(index));
		}
	}
	
	/**
	 * Inserto un terminal que no este repetido
	 * @param Simbolo 
	 */
	public void insertarNoTerminal(Symbol Simbolo){
		if(!this.existeNoTerminal(Simbolo))
			ListaNoTerminales.add(Simbolo);
		
	}
	/**
	 * Inserto un terminal que no este repetido
	 * @param Simbolo
	 */
	
	public void insertarTerminal(Symbol Simbolo){
		if(! this.existeTerminal(Simbolo))
			ListaTerminales.add(Simbolo);
	}
	/**
	 * Inserto una regla que no exista.
	 * @param R regla
	 */
	public void insertarReglas(Regla R){
	   if(! this.existeRegla(R) ){
			ListaReglas.add(R);
		}
	}
	
	/**
	 * Compruebo si existe el syimbolo tipo noterminal en gramatica
	 * @param Simbolo
	 * @return
	 */
	public boolean existeNoTerminal(Symbol Simbolo){
		for(int index = 0; index < ListaNoTerminales.size(); index++){
			if(ListaNoTerminales.get(index).equals(Simbolo)){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Compruebo si existe el syimbolo tipo terminal en gramatica
	 * @param Simbolo
	 * @return
	 */
	public boolean existeTerminal(Symbol Simbolo){
		for(int index = 0; index < ListaTerminales.size(); index++){
			if(ListaTerminales.get(index).equals(Simbolo)){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Compruebo si existe la regla pasado por parametro.
	 * @param R Regla
	 * @return true/false
	 */
	public boolean existeRegla(Regla R){
		return ListaReglas.contains(R);
	}
	
	/**
	 * Obtengo una regla segun la posicion del parametro i.
	 * @param i int
	 * @return
	 */
	public Regla getRegla(int i) {
		return ListaReglas.get(i);
	}
	/**
	 * Optengo el conjunto de reglas que tenga como encabezado de la regla el symbolo pasado por parametro
	 * @param enca symbolo NOTERMINAL
	 * @return Conjunto de regla
	 */
	public ArrayList<Regla> ConjuntoR(NoTerminal enca){
		ArrayList<Regla> salida = new ArrayList<>();
		for(int index = 0; index < ListaReglas.size(); index++){
			if(ListaReglas.get(index).getEncabezado().equals(enca))
				salida.add(ListaReglas.get(index));
		}
		return salida;
	}

	public int numTerminales() {
		return ListaTerminales.size();
	}

	public int numNoTerminales() {
		return ListaNoTerminales.size();
	}
	
	public String salidaT(){
		String salida = "";
		for(int index = 0; index < ListaTerminales.size(); index++){
			salida = salida + ListaTerminales.get(index).getToken().getLexeme() + "\t";
		}
		salida = salida + "\n";
		return salida;
	}
	
	/**
	 * Me devuelve el String de un token almacenado en listaTerminal segun la posicion determinada por symbolo
	 * @param symbolos
	 * @return
	 */
	public String getTerminal(int symbolos) {
		return ListaTerminales.get(symbolos).getToken().getLexeme();
	}
	
	/**
	 * Posicion de la lista de terminales que concuerde con el symbolo buscado.
	 * @param sys
	 * @return
	 */
	public int posT(Symbol sys) {
		for(int index = 0; index < ListaTerminales.size(); index++){
			if(ListaTerminales.get(index).equals(sys))
				return index;
		}
		return -1;
	}
	/**
	 * Numero de reglas.
	 * @return
	 */
	public int numReglas() {
		return ListaReglas.size();
	}
	
	/**
	 * Me devueve el noterminal asociado a una posicion i
	 * @param i
	 * @return
	 */
	public NoTerminal getNTeminales(int i) {
		return (NoTerminal) ListaNoTerminales.get(i);
	}
	
	/**
	 * Me devuelve la posicion en la que se encuentra la regla que coincida con regla_aux
	 * @param regla_aux
	 * @return
	 */
	public int posR(Regla regla_aux) {
		for(int i = 0; i < ListaReglas.size(); i++){
			if(regla_aux.equals(ListaReglas.get(i)))
				return i;	
		}
		return -1;
	}
	
	/**
	 * Posicion de la lista de noterminales que concuerde con el symbolo buscado.
	 * @param sys
	 * @return
	 */
	public int posNT(Symbol symbol) {
		for(int i = 0; i < ListaNoTerminales.size(); i++){
			if(symbol.equals(ListaNoTerminales.get(i)))
				return i;	
		}
		return -1;
	}
	
	/**
	 * Me devueve el noterminal asociado a una posicion pos
	 * @param pos
	 * @return
	 */
	public Terminal getTer(int pos) {
		return (Terminal) ListaTerminales.get(pos);
	}
	
}
