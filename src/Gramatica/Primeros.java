package Gramatica;

import java.util.ArrayList;

public class Primeros {
	private ArrayList <Terminal>  primeros = new ArrayList<>();
	private ArrayList <NoTerminal>  NT = new ArrayList<>();
	private Gramatica gramatica;
	
	public Primeros(Gramatica gramatica){
		this.gramatica = gramatica;
	}
	
	public void primeros(NoTerminal NT){
			setup(NT);
	}
	
	public void setup(NoTerminal symbol){
		ArrayList<Regla> conR = gramatica.ConjuntoR((NoTerminal) symbol);		// TENGO TODAS LAS REGLAS CON ESE ENCABEZADO.
		for(int index = 0; index < conR.size(); index++){
			if(conR.get(index).numEl()> 0 && conR.get(index).getElemento(0) instanceof Terminal){
				this.primeros.add((Terminal) conR.get(index).getElemento(0));
			}
			else if (conR.get(index).numEl()> 0 && !NT.contains(symbol)){
				this.NT.add((NoTerminal) symbol);
				setup((NoTerminal) conR.get(index).getElemento(0));
			}
		}
	}
	
	public ArrayList<Terminal> getPrimeros(){
		return primeros;
	}
	
}
