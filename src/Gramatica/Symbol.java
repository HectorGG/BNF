package Gramatica;

import Lexico.Token;

public abstract class Symbol {
	private  Token Ter;
	
	public Symbol(Token Ter){
		this.Ter = Ter;
	}
	
	public Token getToken(){
		return Ter;
	}
	
	public void setToken(Token T){
		this.Ter = T;
	}
	public boolean equals(Symbol s){
		return  Ter.getLexeme().equals(s.getToken().getLexeme());
	}
	public String toString(){
		return Ter.getLexeme() + " ";
	}

	public boolean esAceptar() {
		if(this.Ter.getKind() == Token.EOF)
			return true;
		else
			return false;
	}
}
