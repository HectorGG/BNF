package Gramatica;

import Lexico.Token;

public class Terminal extends Symbol{

	public Terminal(Token Ter) {
		super(Ter);
	}
	public String Str(){
		String salida = getToken().getLexeme();
		salida = salida.replaceAll("[<->]", "");
		return salida;
	}
	
	
}
