package Gramatica;

import java.util.ArrayList;

import Lexico.Token;

public class Regla {
	private Symbol noterminal;
	private ArrayList<Symbol> Elementos = new ArrayList<Symbol>();
	private int pos_punto;
	
	
	public Regla(Symbol en) {
		this.noterminal = en;
		this.pos_punto = 0;
	}

	/**
	 * GENERA UNA REGLA TIPO <X> -> .<S>
	 * @param 
	 * @return   
	 */
	
	public Regla(Symbol symbol, int nul) {
		Token T = new Token(0, "<X>", 0, 0);
		Token end = new Token(Token.EOF, "<EOF>", 0, 0);
		this.noterminal = new NoTerminal(T);
		this.a�adirSymbol(symbol);
		this.a�adirSymbol(new Terminal(end));
		pos_punto = 0;

	}
	
	public Regla(Regla regla) {
		this.noterminal = regla.getEncabezado();
		this.pos_punto = regla.pos_punto;
		this.Elementos = regla.Elementos;
		
	}
	/**
	 * Crea una regla a partir del encabezado y del conjunto de symbolos que iran a su izquierda.
	 * 
	 * @param encabezado  Predicado de la regla
	 * @param parseReglas conjunto de sinbolos
	 */
	public Regla(NoTerminal encabezado, ArrayList<Symbol> parseReglas) {
		this.Elementos = parseReglas;
		this.noterminal = encabezado;
	}

	/**
	 * Insertar un simbolo por la derecha
	 * @param Sym Symbolo.
	 */
	public void a�adirSymbol(Symbol Sym) {
		Elementos.add(Sym);
	}
	/**
	 * @return Me devulve el encabezado de una regla.
	 */
	public Symbol getEncabezado() {
		return noterminal;
	}
	
	/**
	 * @return Me devulve el symbolo consumido
	 */

	public Symbol Elemento_Consumido() {
		return Elementos.get(pos_punto);
	}
	
	/**
	 * @return Me devulve la regla ya consumida.
	 */
	public  Regla Consumir(){
		Regla Raux = new Regla(this);
		Raux.pos_punto = this.pos_punto + 1;
		return Raux;
	}
	/**
	 * 
	 * @return true si todavia existe algun elemento a consumir de la regla.
	 */
	public boolean se_consume() {
		return pos_punto < Elementos.size();
	}
	

	public String toString() {
		String salida = noterminal.toString() + " -> ";
		for(int indice = 0; indice < this.Elementos.size(); indice++){
			if(pos_punto == indice)
				salida = salida + ".";
			salida = salida + Elementos.get(indice);
		}
		return salida;
	}
	
	public boolean equals(Regla R1){
		return R1.Elementos.containsAll(Elementos) && noterminal.equals(R1.noterminal) && pos_punto == R1.pos_punto 
				&& Elementos.size() == R1.Elementos.size();
	}
	/**
	 * Resetea el punto de la regla.
	 */
	public void resetPunto(){
		this.pos_punto = 0;
	}
	/**
	 * 
	 * @param i posicion del elemento que se quiere optener.
	 * @return Symbolo 
	 */
	public Symbol getElemento(int i) {
		return Elementos.get(i);
	}
	/**
	 * @return Numero de elementos que tiene la regla.
	 */
	public int numEl() {
		return Elementos.size();
	}
	/**
	 * 
	 * @param parseReglas_p conjunto de symbolos que se desea a�adir a la regla.
	 */
	public void a�adirSymbol(ArrayList<Symbol> parseReglas_p) {
		this.Elementos.addAll(parseReglas_p);
	}

}
