package Sintactico;

import java.util.ArrayList;

import Gramatica.Gramatica;
import Gramatica.NoTerminal;
import Gramatica.Regla;
import Gramatica.Symbol;
import Gramatica.Terminal;
import Lexico.BNFLexer;
import Lexico.Token;
import Lexico.TokenConstants;

/**
 * 
 * @author Hector Gonzalez Guerreiro
 * Clase que me gestiona la parte semantica y sintactica del proyecto
 *
 */
public class BNFparse implements TokenConstants {
	private BNFLexer lexer;
	private Token nextToken;
	private Gramatica gramatica;
	
	public boolean parse(String args) {
		try {
			this.lexer = new BNFLexer(args);
			this.nextToken = lexer.getNextToken();
			parseCompilationUnit();
			if (nextToken.getKind() == EOF) {
				return true;
			} else
				return false;
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return false;
		}
	}
	
	public Gramatica getGramatica(){
		return gramatica;
	}
	
	/**
	 * 
	 * @param kind le paso la id asociado a cada tokenConst ya definido
	 * @return Me devulve el token asociado, confirmando que ese es el token que debe ser.
	 * @throws SintaxException
	 */
	private Token match(int kind) throws SintaxException {
		Token salida = nextToken;
		if (nextToken.getKind() == kind){
			nextToken = lexer.getNextToken();
			return salida;
		}
		else
			throw new SintaxException(nextToken, kind);
	}
	
	/**
	 * Primer metodo que inicia el analisis.
	 * @throws SintaxException
	 */
	private void parseCompilationUnit() throws SintaxException {
		int[] expected = { NOTERMINAL };
		switch (nextToken.getKind()) {
		case NOTERMINAL:
			gramatica = new Gramatica(parseDefinicion_prima());
			break;
		default:
			throw new SintaxException(nextToken, expected);
		}
	}
	/**
	 * Este metodo obtengo las n reglas que contiene la semantica
	 * @return Conjunto de reglas
	 * @throws SintaxException
	 */
	private ArrayList<Regla> parseDefinicion_prima() throws SintaxException {
		int[] expected = { NOTERMINAL };
		ArrayList<Regla> reglas = new ArrayList<>();
		switch (nextToken.getKind()) {
		case NOTERMINAL:
			reglas.addAll(parseDefinicion());
			reglas.addAll(parseDefinicion_prima());
			break;
		case EOF:
			break;
		default:
			throw new SintaxException(nextToken, expected);
		}
		return reglas;
	}
	/**
	 *  
	 * @return devuelvo el conjunto de reglas.
	 * @throws SintaxException
	 */
	private ArrayList<Regla> parseDefinicion() throws SintaxException {
		int[] expected = { NOTERMINAL };
		ArrayList<Regla> reglas = new ArrayList<>();
		NoTerminal NT = null;
		switch (nextToken.getKind()) {
		case NOTERMINAL:
			NT = new  NoTerminal(match(NOTERMINAL));
			match(EQ);
		    reglas.addAll(parseRegla(new Regla(NT)));
			match(SEMICOLON);
			break;
		default:
			throw new SintaxException(nextToken, expected);
		}
		return reglas;
	}
	
	/**
	 * 
	 * @param R Le paso una variable regla por herencia
	 * @return Devuelvo la variable sintetizada que contine reglas
	 * @throws SintaxException
	 */

	private ArrayList<Regla> parseRegla(Regla R) throws SintaxException {
		int[] expected = { NOTERMINAL, TERMINAL, BAR};
		ArrayList<Regla> reglas = new ArrayList<>();
		switch (nextToken.getKind()) {
		case TERMINAL:
		case NOTERMINAL:
			reglas.add(parseReglas(R));
			reglas.addAll(parseCReglas((NoTerminal) R.getEncabezado()));
		case BAR:
			break;
		default:
			throw new SintaxException(nextToken, expected);
		}
		return reglas;
	}
	/**
	 * 
	 * @param encabezado La parte Izquierda de la regla
	 * @return	Devuelvo la variable sintetizada que contine reglas
	 * @throws SintaxException
	 */
	private ArrayList<Regla> parseCReglas (NoTerminal encabezado) throws SintaxException {
		int[] expected = { BAR, SEMICOLON };
		ArrayList<Regla> reglas = new ArrayList<>();
		switch (nextToken.getKind()) {
		case BAR:
			match(BAR);
			reglas.add(parseReglas(new Regla(encabezado)));
			reglas.addAll(parseCReglas(encabezado));
			break;
		case SEMICOLON:
		case EOF:
			break;
		default:
			throw new SintaxException(nextToken, expected);
		}
		return reglas;

	}
	
	/**
	 * 
	 * @param R Le paso una variable heredada de tipo regla
	 * @return  Devuelvo la variable sintetizada de tipo regla <A> -> a b <A>
	 * @throws SintaxException
	 */

	private Regla parseReglas(Regla R) throws SintaxException {
		int[] expected = { NOTERMINAL, TERMINAL, SEMICOLON };
		Regla r = new Regla(R);
		switch (nextToken.getKind()) {
		case NOTERMINAL:
		case TERMINAL:
			r.a�adirSymbol(parseReglas_p());
			break;
		case SEMICOLON:
		case BAR:
			break;
		default:
			throw new SintaxException(nextToken, expected);
		}
		return r;

	}
	
	/** 
	 * @return el conjunto de simbolos seguidos (HASTA QUE APARECE | )
	 * @throws SintaxException
	 */
	
	private ArrayList<Symbol> parseReglas_p() throws SintaxException {
		int[] expected = { NOTERMINAL, TERMINAL, BAR, SEMICOLON  };
		ArrayList<Symbol> Conjunto_Symbolos = new ArrayList<>();
		switch (nextToken.getKind()) {
		case NOTERMINAL:
			Conjunto_Symbolos.add(parseNoterminal());
			Conjunto_Symbolos.addAll(parseReglas_p());
			break;
		case TERMINAL:
			Conjunto_Symbolos.add(parseTerminal());
			Conjunto_Symbolos.addAll(parseReglas_p());
			break;
		case BAR:
		case SEMICOLON:
			break;
		default:
			throw new SintaxException(nextToken, expected);
		}
		
		return Conjunto_Symbolos;
	}
	/**
	 * 
	 * @return Devuelvo el symbolo Terminal.
	 * @throws SintaxException
	 */
	private Terminal parseTerminal() throws SintaxException {
		int[] expected = { TERMINAL };
		Terminal Ter = null;
		switch (nextToken.getKind()) {
		case TERMINAL:
		    Ter = new Terminal(match(TERMINAL));
			break;
		default:
			throw new SintaxException(nextToken, expected);
		}
		return Ter;

	}
	/**
	 * 
	 * @return Devuelvo el symbolo NoTerminal.
	 * @throws SintaxException
	 */
	private NoTerminal parseNoterminal() throws SintaxException {
		int[] expected = { NOTERMINAL };
		NoTerminal NT = null;
		switch (nextToken.getKind()) {
		case NOTERMINAL:
			NT = new NoTerminal(match(NOTERMINAL));
			break;
		default:
			throw new SintaxException(nextToken, expected);
		}
		
		return NT;

	}

}
