package Lexico;
/**
* define las constantes asociadas a las 
* diferentes categorias lexicas del lenguaje
**/

public interface TokenConstants {
	 public int EOF = 0;
	 public int NOTERMINAL = 1;
	 public int TERMINAL = 2;
     public int SEMICOLON = 3;
     public int BAR = 4;
     public int EQ = 5;
}