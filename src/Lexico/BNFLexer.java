package Lexico;

import java.io.IOException;



/**
 * Clase que desarrolla el funcionamiento de una M�quina Discriminadora
 * Determinista
 * 
 * @author Hector Gonzalez Guerreiro
 *
 */
public class BNFLexer extends Lexer implements TokenConstants {
	/**
	 *  Transiciones del automata lexico
	 *  @param Estado inicial.
	 *  @param Simbolo del alfabeto.
	 *  @param Estado final.
	 *   
	 */
	@Override
	protected int transition(int state, char symbol) {
		switch(state){
			case 0:
				if(symbol == '/') return 1;
				else if(symbol == ' ' || symbol == '\t') return 7;
				else if(symbol == '\r' || symbol == '\n') return 7;
				else if(symbol >= 'a' && symbol <= 'z') return 8;
				else if(symbol >= 'A' && symbol <= 'Z') return 8;
				else if(symbol == '_') return 8;
				else if(symbol == '<') return 9;
				else if(symbol == ':') return 12;
				else if(symbol == '|') return 16;	//Final//
				else if(symbol == ';') return 17;	//Final/
				else return -1;
			case 1:
				if(symbol == '*') return 2;
				else return -1;
			case 2:
				if(symbol == '*') return 3;
				else return 2;
			case 3:
				if(symbol == '*') return 3;
				else if(symbol == '/') return 4; //FINAL//
				else return 2;
			case 5:
				if(symbol == '\n') return 6;
				else return 5;
			case 7:
				if(symbol == ' ' || symbol == '\t') return 7;
				else if(symbol == '\r' || symbol == '\n') return 7;	//FINAL//
				else return -1;
			case 8:
				if(symbol >= 'a' && symbol <= 'z') return 8;
				else if(symbol == '_') return 8;
				else if(symbol >= 'A' && symbol <= 'Z') return 8;
				else if(symbol >= '0' && symbol <= '9') return 8;
				else return -1;
			case 9:
			    if(symbol >= 'a' && symbol <= 'z') return 10;
				else if(symbol >= 'A' && symbol <= 'Z') return 10;
				else if(symbol == '_') return 10;
				else return -1;
			case 10:
				if(symbol >= 'a' && symbol <= 'z') return 10;
				else if(symbol >= 'A' && symbol <= 'Z') return 10;
				else if(symbol >= '0' && symbol <= '9') return 10;
				else if(symbol == '_') return 10;
				else if(symbol == '>') return 11;				//F//
				else return -1;
			case 12: 
				if(symbol == ':') return 13;
				else return -1;
			case 13:
				if(symbol == '=') return 14;		//F//
				else return -1;
			default:
				return -1;
		}
		
	}
	/**
	 * Verifica si un estado es final
	 * 
	 * @param state Estado
	 * @return true, si el estado es final
	 */
	
	@Override
	protected boolean isFinal(int state) {
		if(state <=0 || state > 18) return false;
		switch(state) {		
		case 1:
		case 2:
		case 3:
		case 5:
		case 6:
		case 9:
		case 10:
		case 12:
		case 13:
				return false;
			default: 
				return true;
		}
	}

	@Override
	protected Token getToken(int state, String lexeme, int row, int column) {
		switch (state) {
		case 4:  return null;
		case 7:  return null;
		case 11: return new Token(TERMINAL, lexeme, row, column);
		case 8:  return new Token(NOTERMINAL, lexeme, row, column);
		case 14: return new Token(EQ, lexeme, row, column);
		case 16: return new Token(BAR, lexeme, row, column);
		case 17: return new Token(SEMICOLON, lexeme, row, column);
			default: return null;
		}
		
	}
	
	/**
	 * Constructor
	 * @param filename Nombre del fichero fuente
	 * @throws IOException En caso de problemas con el flujo de entrada
	 */
	public BNFLexer(String filename) throws IOException {
		super(filename);
	}
	
}
