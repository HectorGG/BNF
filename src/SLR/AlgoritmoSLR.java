package SLR;

import java.util.ArrayList;

import AST.MaquinaEstados;
import Gramatica.Gramatica;
import Gramatica.NoTerminal;
import Gramatica.Regla;
import Gramatica.Siguientes;
import Gramatica.Symbol;
import Gramatica.Terminal;


public class AlgoritmoSLR {
	private String Tabla [][];
	private String Tabla_2 [][];
	private MaquinaEstados MA;
	private Gramatica gramatica;
	private int numSymbolos , numEstados;
	
	/**
	 * Constructor del genereado de tabla.
	 * @param G Gramatica de los datos.
	 * @param MA Maquina de los conjuntos de estados.
	 */
	public AlgoritmoSLR(Gramatica G, MaquinaEstados MA){
	    numSymbolos = G.numTerminales();
		numEstados = MA.numConjuntos();
		Tabla = new String[numEstados][numSymbolos];
		Tabla_2 = new String[numEstados][G.numNoTerminales()];
		
		for(int Estados = 0; Estados < numEstados; Estados++ ){
			for(int Symbolos = 0; Symbolos < numSymbolos; Symbolos++){
					Tabla[Estados][Symbolos] = "";
			}
		}
		
		for(int Estados = 0; Estados < numEstados; Estados++ ){
			for(int Symbolos = 0; Symbolos < G.numNoTerminales(); Symbolos++){
					Tabla_2[Estados][Symbolos] = "";
			}
		}
		
		this.MA = MA;
		this.gramatica = G;
		setup();
		
	}
	/**
	 * Creacion de la tabla de desplazamiento y reduccion.
	 */
	public void setup(){
		
		
		for(int index = 0; index < MA.numConjuntos(); index++){
			for(int trans = 0; trans < MA.getConjunto(index).numTrans(); trans++ ){
				// DETECTAR LOS DESPLAZAMIENTOS //
				
				Symbol sys = MA.getConjunto(index).getAristas(trans).getSymbol();
				if(!MA.getConjunto(index).getAristas(trans).esFinal()){
					if(gramatica.posT(sys) >= 0)
						Tabla[index][gramatica.posT(sys)] ="D_" + MA.getConjunto(index).getAristas(trans).getConjunto().getId();
				}
				else{
					Tabla[index][trans] = "OK";
				}
			}
				// DETECTAR LAS REDUCCIONES //
			for(int reg = 0; reg < MA.getConjunto(index).numR(); reg++){
				if(!MA.getConjunto(index).getRegla(reg).se_consume()){
					Regla regla_aux = new Regla(MA.getConjunto(index).getRegla(reg));
					regla_aux.resetPunto();
					Siguientes sig = new Siguientes(gramatica, (NoTerminal) regla_aux.getEncabezado());
					ArrayList <Terminal> siguientes = sig.getSiguiente();
					for(int i = 0; i < siguientes.size(); i++)
						Tabla[index][gramatica.posT(siguientes.get(i))] = "R_" + String.valueOf(gramatica.posR(regla_aux));
					
				}
			}
			
			//CREAR LA SEGUNDA TABLA CON LOS NOTERMINALES //
			for(int trans = 0; trans < MA.getConjunto(index).numTrans(); trans++){
				if(!MA.getConjunto(index).getAristas(trans).esFinal() && MA.getConjunto(index).getAristas(trans).getSymbol() instanceof NoTerminal){
					Tabla_2[index][gramatica.posNT(MA.getConjunto(index).getAristas(trans).getSymbol())] = String.valueOf(MA.getConjunto(index).getAristas(trans).getConjunto().getId());
				}
			}
		}
		
	}
	
	public String toString(){
		String salida = "";
		
		for(int indice = 0; indice <numEstados; indice++){
			for(int indice2 = 0; indice2 < numSymbolos; indice2++){
				salida = salida + Tabla[indice][indice2];
			}
			salida = salida + "\n";
		}
		return salida;
	}

	public String[][] getTabla_1() {
		return Tabla;
	}

	public String[][] getTabla_2() {
		return Tabla_2;
	}
	
}