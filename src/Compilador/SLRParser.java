package Compilador;
import java.util.Stack;

/**
 * Clase que desarrolla el comportamiento com�n de los analizadores
 * sint�cticos ascendentes basados en tablas SLR
 * 
 * @author Francisco Jos� Moreno Velo
 *
 */
public abstract class SLRParser {

	/**
	 * Analizador l�xico
	 */
	private Lexer lexer;
	
	/**
	 * Siguiente token de la cadena de entrada
	 */
	private Token nextToken;
	
	/**
	 * Pila de estados
	 */
	private Stack<Integer> stack;
	
	/**
	 * Tabla de acciones
	 */
	protected ActionElement[][] actionTable;
	
	/**
	 * Tabla de Ir_a
	 */
	protected int[][] gotoTable;
	
	/**
	 * Lista de reglas
	 */
	protected int[][] rule;
	
	/**
	 * Realiza el an�lisis sint�ctico a partir del l�xico
	 * @param filename
	 * @return
	 */
	protected boolean parse(Lexer lexer) {
		try{
			this.lexer = lexer;
			this.nextToken = lexer.getNextToken();
			this.stack = new Stack<Integer>();
			this.stack.push(new Integer(0));
			while(true) {
				if(step()) break;
			}
			return true;
		} catch(Exception ex) {
			System.out.println(ex.toString());
			return false;
		}
	}
	
	/**
	 * M�todo que realiza una acci�n de desplazamiento
	 * @param action
	 */
	private void shiftAction(ActionElement action) {
		nextToken = lexer.getNextToken();
		stack.add(new Integer(action.getValue()));
	}
	
	/**
	 * M�todo que realiza una acci�n de reducci�n
	 * @param action
	 */
	private void reduceAction(ActionElement action) {
		int ruleIndex = action.getValue();
		int numSymbols = rule[ruleIndex][1];
		int leftSymbol = rule[ruleIndex][0];
		while(numSymbols > 0) {
			stack.pop();
			numSymbols --;
		}
		int state = ((Integer) stack.lastElement()).intValue();
		int gotoState = gotoTable[state][leftSymbol];
		stack.push(new Integer(gotoState));
	}
	
	/**
	 * Ejecuta un paso en el an�lisis sint�ctico, es decir, extrae
	 * un elemento de la pila y lo analiza.
	 * @throws SintaxException
	 */
	private boolean step() throws SintaxException {
		int state = ((Integer) stack.lastElement()).intValue();
		ActionElement action = actionTable[state][nextToken.getKind()];
		if(action == null) {
			int count = 0;
			for(int i=0; i<actionTable[state].length; i++) if(actionTable[state][i] != null) count++;
			int[] expected = new int[count];
			for(int i=0,j=0; i<actionTable[state].length; i++) if(actionTable[state][i] != null) {
				expected[j] = i;
				j++;
			}
			throw new SintaxException(nextToken,expected);
		}
		int actionType = action.getType();
		if(actionType == ActionElement.ACCEPT) {
			return true;
		} else if(actionType == ActionElement.SHIFT) {
			shiftAction(action);
			return false;
		} else if(actionType == ActionElement.REDUCE) {
			reduceAction(action);
			return false;
		}
		return false;
	}
}
