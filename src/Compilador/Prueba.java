package Compilador;

import java.io.IOException;

public class Prueba {

	public static void main(String[] args) {
		try {
			ExprLexer Lexico = new ExprLexer(args[0]);
			Parser parser = new Parser();
			if (parser.parse(Lexico)) {
				System.out.println("Correcto");		
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}

	}

}
