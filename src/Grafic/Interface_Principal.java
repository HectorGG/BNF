package Grafic;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import BNF.BNF;

import javax.swing.JTextField;
import javax.swing.JLayeredPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import javax.swing.JFileChooser;
import java.awt.TextArea;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class Interface_Principal extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField Directorio;
	private File file;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface_Principal frame = new Interface_Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Interface_Principal() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Interface_Principal.class.getResource("/Imagenes/I2.png")));
		setTitle("Compilador SLR\r\n");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 605, 334);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JLayeredPane layeredPane = new JLayeredPane();
		contentPane.add(layeredPane, BorderLayout.CENTER);
		
		Directorio = new JTextField();
		Directorio.setBounds(10, 11, 186, 20);
		layeredPane.add(Directorio);
		Directorio.setColumns(10);
		
		TextArea textArea = new TextArea();
		textArea.setBounds(10, 87, 380, 176);
		layeredPane.add(textArea);
		
		JButton Browser = new JButton("Buscar");
		Browser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		         
		        if(e.getSource() == Browser){
		            final JFileChooser jFileChooser = new JFileChooser("./");
		            int returnVal = jFileChooser.showOpenDialog(getParent());
		            if(returnVal == JFileChooser.APPROVE_OPTION){
		                file = jFileChooser.getSelectedFile();
		                textArea.setText("Selected file: " + file.getName());
		                Directorio.setText(file.getAbsolutePath());
		            }else if(returnVal == JFileChooser.CANCEL_OPTION){
		                textArea.setText("Cancelled");
		            }else if(returnVal == JFileChooser.ERROR_OPTION){
		                textArea.setText("Error!");
		            }else{
		                textArea.setText("unknown...");
		            }
		        }
		    }
		});
		Browser.setBounds(263, 10, 113, 21);
		layeredPane.add(Browser);
		
		JButton Aceptar = new JButton("Aceptar");
		Aceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("unused")
				BNF INICIO = new BNF(file,textArea);
			}
		});
		Aceptar.setBounds(263, 42, 113, 23);
		layeredPane.add(Aceptar);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(Interface_Principal.class.getResource("/Imagenes/ima.jpg")));
		lblNewLabel.setBounds(396, 11, 173, 252);
		layeredPane.add(lblNewLabel);
		
		
		
		
	}
}
