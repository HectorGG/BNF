package BNF;

import java.io.FileWriter;
import java.io.PrintWriter;

import AST.MaquinaEstados;
import Gramatica.Gramatica;
import SLR.AlgoritmoSLR;

public class Generador_Codigo {
	private Gramatica gr;
	private String TokenConstant;
	private String SymbolConstant;
	private String fis[] = { "TokenConstants.java", "SymbolConstants.java","Parser.java"};
	private String parse;
	private MaquinaEstados maquina;
	private AlgoritmoSLR slr;

	public Generador_Codigo(Gramatica g, MaquinaEstados MA, AlgoritmoSLR SLR) {
		this.gr = g;
		this.maquina = MA;
		this.slr = SLR;
		this.generarTokenConstant();
		this.generarSymbolConstant();
		this.generarParser();
		this.EscribirFicheros();
	}

	public void generarTokenConstant() {
		TokenConstant = "package Compilador; \n public interface TokenConstants { \n";
		for (int pos = 0; pos < gr.numTerminales(); pos++)
			TokenConstant = TokenConstant + "public int " + gr.getTer(pos).Str() + " = " + gr.posT(gr.getTer(pos))
					+ ";\n";
		TokenConstant = TokenConstant + "}\n";

	}

	public void generarSymbolConstant() {
		SymbolConstant = " package Compilador; \n public interface SymbolConstants { \n";
		for (int pos = 0; pos < gr.numNoTerminales(); pos++)
			SymbolConstant = SymbolConstant + "public int " + gr.getNTeminales(pos).getToken().getLexeme() + " = "
					+ gr.posNT(gr.getNTeminales(pos)) + ";\n";
		SymbolConstant = SymbolConstant + "}\n";
	}

	public void EscribirFicheros() {
		FileWriter fichero = null;
		PrintWriter pw = null;

		for (int numF = 0; numF < fis.length; numF++) {
			try {
				// Apertura del fichero y creacion de BufferedReader para poder
				// hacer una lectura comoda (disponer del metodo readLine()).
				fichero = new FileWriter("./src/Compilador/" + fis[numF]);
				pw = new PrintWriter(fichero);

				switch (numF) {
				case 0:
					pw.println(TokenConstant);
					break;
				case 1:
					pw.println(SymbolConstant);
					break;
				case 2:
					pw.println(parse);
					break;
				}

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					// Nuevamente aprovechamos el finally para
					// asegurarnos que se cierra el fichero.
					if (null != fichero)
						fichero.close();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		}
	}

	public void generarParser() {
		parse = "";
		parse = parse + "package Compilador; " + "\n" +" public class Parser extends SLRParser implements TokenConstants, SymbolConstants { \n"
				+ " public Parser() { \n " + "  initRules(); \n" + "initActionTable(); \n" + "initGotoTable(); \n"
				+ "} \n \n" + "private void initRules() { \n" + "int[][] initRule = { \n" + "{0 , 0},\n";
		for(int i = 1; i < gr.numReglas();i++){
			parse = parse + "{" + gr.getRegla(i).getEncabezado() + "," + gr.getRegla(i).numEl() + "},\n";
		}
		parse = parse + "}; \n this.rule = initRule; \n } \n";
		
		parse = parse + "private void initActionTable(){ \n"
				+ "actionTable = new ActionElement["+maquina.numConjuntos() +"]["+gr.numTerminales()+"]; \n";
		
		for(int i = 0; i < maquina.numConjuntos(); i++ ){
			for(int j = 0; j < gr.numTerminales(); j++){
				if(slr.getTabla_1()[i][j] != ""){
					if(slr.getTabla_1()[i][j].charAt(0) == 'D')
						parse = parse + "\t actionTable["+i+"]["+gr.getTer(j).Str()+"] = new ActionElement(ActionElement." + "SHIFT," + slr.getTabla_1()[i][j].substring(2,slr.getTabla_1()[i][j].length() ) + "); \n";
					else if(slr.getTabla_1()[i][j].charAt(0) == 'R')
						parse = parse + "\t actionTable["+i+"]["+gr.getTer(j).Str()+"] = new ActionElement(ActionElement." + "REDUCE," +  slr.getTabla_1()[i][j].substring(2,slr.getTabla_1()[i][j].length() ) + "); \n";
					else if(slr.getTabla_1()[i][j] == "OK")
						parse = parse + "\t actionTable["+i+"]["+gr.getTer(j).Str()+"] = new ActionElement(ActionElement." + "ACCEPT,0); \n";
			}
			}
			parse = parse + " \n";
		}
		parse = parse + "} \n";
		parse = parse + "private void initGotoTable(){ \n"
				+ "gotoTable = new int[" + maquina.numConjuntos()+"][" + gr.numNoTerminales() + "]; \n";
		
		for(int i = 0; i < maquina.numConjuntos(); i++ ){
			for(int j = 0; j < gr.numNoTerminales(); j++){
				if(slr.getTabla_2()[i][j] != ""){
					parse = parse + "\t gotoTable[" + i +"][" + gr.getNTeminales(j) +"] = " + slr.getTabla_2()[i][j] + "; \n";
				}
			}
		}
		parse = parse + "} \n  \n";
		
		parse = parse + " public boolean Parser(String filename){ \n" 
				+ " \t try { \n " 
				+ " \t \t return parse(new ExprLexer(filename)); \n"
				+ " \n \t \t } catch (Exception ex) { \n "
				+ " \t \t System.out.println(ex.getMessage()); "
				+ " \n \t return false; \n } \n } \n }";
		
		
	}
}
