package BNF;

import java.awt.Color;
import java.awt.TextArea;
import java.io.File;
import java.io.IOException;

import AST.MaquinaEstados;
import Lexico.BNFLexer;
import SLR.AlgoritmoSLR;
import Sintactico.BNFparse;

public class BNF {

	public BNF(File file, TextArea textArea) {
		try {
			textArea.setText("-------------------EMPEZANDO EL ANALISIS DEL TEXTO----------------\n");
			@SuppressWarnings("unused")
			BNFLexer Lexico = new BNFLexer(file.getPath());
			textArea.setText( textArea.getText() + "Finalizado el proceso Lexico.\n");
			BNFparse parser = new BNFparse();
			textArea.setText( textArea.getText() + "Finalizado el proceso semantico.\n");
			if (parser.parse(file.getPath())) {
				MaquinaEstados m = new MaquinaEstados(parser.getGramatica());
				textArea.setText( textArea.getText() + "Finalizado el proceso Sintactico Y de la maquina de estados.\n");
				AlgoritmoSLR SLR = new AlgoritmoSLR(parser.getGramatica(), m);
				textArea.setText( textArea.getText() + "Finalizado el proceso SLR.\n");
				@SuppressWarnings("unused")
				Generador_Codigo generador = new Generador_Codigo(parser.getGramatica(),m,SLR);
				textArea.setText( textArea.getText() + "Finalizado el proceso los archivos se han creado correctamente.\n");
			
			} else {
				textArea.setText(textArea.getText() + "Incorrecto \n");
			}
		} catch (IOException e) {
			textArea.setBackground(Color.RED);
			textArea.setText(e.getMessage());
			textArea.setBackground(Color.BLACK);
		}
	}

}
